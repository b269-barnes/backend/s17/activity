/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printUserDetails(){
	
	let fullName = prompt("What is your full name?"); 
	let age = prompt("How old are you?"); 
	let location  = prompt("Where do you live?");


	console.log("Hello, "+ fullName)
	console.log("You are " + age + " years old." ); 
	console.log("You live in "+ location); 
	alert("Thank you for the response!")
};

printUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:


function displayFavBand (){

	console.log("1. The Beatles");
	console.log("2. Metallica");
	console.log("3. The Eagles");
	console.log("4. L'arc~en~Ciel");
	console.log("5. Eraserheads");
}

displayFavBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function displayFavMovies (){
	let movie1 = "1. The God Father"
	let rating1 = "97%"
	let movie2 = "2. The God Father, Part II"
	let rating2 = "96%"
	let movie3 = "3. Shawshank Redemption"
	let rating3 = "91%"
	let movie4 = "4. To Kill A Mockingbird"
	let rating4 = "93%"
	let movie5 = "5. Psycho"
	let rating5 = "96%"


	console.log(movie1)
	console.log("Rotten Tomatoes ratings: " + rating1)
	console.log(movie2)
	console.log("Rotten Tomatoes ratings: " + rating2)
	console.log(movie3)
	console.log("Rotten Tomatoes ratings: " + rating3)
	console.log(movie4)
	console.log("Rotten Tomatoes ratings: " + rating4)
	console.log(movie5)
	console.log("Rotten Tomatoes ratings: " + rating5)

}
displayFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function friends(){
	let friend1 = ("James Bucky"); 
	let friend2 = ("Natasha Ramanoff"); 
	let friend3 = ("Bruce Banner");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

friends();